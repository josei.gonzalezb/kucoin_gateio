#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  8 17:43:02 2021

@author: jose
"""
from kucoin_gateio import *
from time import sleep



def bot_loop(scrapper, gate):
    coin_to_buy = None
    coin_to_buy = scrapper.scrap()
    sleep(3)
    
    if coin_to_buy: # Check if there are new coins announced
        scrapper.logger("New coin detected: " + str(coin_to_buy))
        print("New coin detected: " + str(coin_to_buy))
        
        try:
            gate.buy_coin(coin_to_buy) 
        except:
            print("Error occurred, see the log!")  
    return      

def main():
    scrapper = kucoin_scrapper()
    gate = gateio()
    
    while(True):
        try:
            bot_loop(scrapper, gate)
        except KeyboardInterrupt:
            break
        except:
            scrapper.logger("An error has occurred, restarting...")
            print("An error has occurred, restarting...")
                
            del scrapper, gate
            main()
            
if __name__ == '__main__':
    main()
        

