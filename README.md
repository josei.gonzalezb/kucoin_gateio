# kucoin_gateio

Trading bot that buys a cryptocurrency on gate.io when it is anounced to get listed on kucoin.com

## Install and config

1. Clone repository.

2. Install requirements with: 

```
pip install -r requirements.txt 
```

NOTE: You must have pip installed.

2. Download chromedriver from here: https://chromedriver.chromium.org/ 

NOTE: The version must match your installed chrome browser version.

3. Edit cfg.ini with the absolute path to the chromedriver file and your gate.io API keys. Change use_api_keys = 1. You can also edit
the expected profit, but for now it's not recommended.

4. Run the bot with:

```
python3 main.py
```




