#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  8 11:37:52 2021

@author: jose
"""
from selenium import webdriver
from selenium.webdriver.chrome.options import Options 
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

import gate_api
from gate_api import Order
from gate_api.exceptions import ApiException, GateApiException

import configparser
from time import sleep
from datetime import datetime


CONFIG_FILE = 'cfg.ini'

class kucoin_scrapper():
    def __init__(self):
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)
        self.chromedriver_path = config['GENERAL']['chromedriver_path']
        
        self.driver = self.init_driver()
        self.tickers_list_last = self.get_tickers_list()
        self.tickers_to_buy = []
        self.tickers_list = self.tickers_list_last.copy()
    
    def init_driver(self):
        chrome_options = Options()  
        chrome_options.add_argument("--headless") 
        driver = webdriver.Chrome(executable_path=self.chromedriver_path, options=chrome_options)
        return driver

    def get_items_text(self, coin_list):
        raw_text = []
        for coin in coin_list:
            raw_text.append(coin.text)
        return raw_text

    def get_lines(self, string):
        n = string.count('\n') + 1
        return n

    def get_ticker(self, string):
        n_lines = self.get_lines(string)
        lines = string.split('\n')

        if n_lines == 3:
            string = lines[0]
        else:
            string = lines[1]

        start = string.find('(')
        end = string.find(')')
        # Check if the announcement is a listing announcement 
        if(string.find('Gets Listed') != -1): 
            ticker = string[start+1:end]
            return ticker
        else:
            return None
        

    def get_tickers_list(self):
        self.driver.get("https://www.kucoin.com/news/categories/listing")
        WebDriverWait(self.driver, 50).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'item___2ffLg'))
            )
            
        coin_items = self.driver.find_elements_by_class_name('item___2ffLg')

        items_text = self.get_items_text(coin_items)

        tickers_list = []
        for item in items_text:
            ticker = self.get_ticker(item)
            if(ticker): tickers_list.append(ticker)

        return tickers_list

    ## Compares new tickers list with the last saved
    def get_new_tickers(self):
        new_tickers = None
        if self.tickers_list_last != self.tickers_list: # New tickers added on kucoin web
            end = self.tickers_list.index(self.tickers_list_last[0])
            new_tickers = self.tickers_list[0:end]

        return new_tickers
    
    def return_n_clear(self):
        try:
            temp = self.tickers_to_buy[0]
            self.tickers_to_buy.clear()
            return temp
        except TypeError:
            print("No new coins detected")
            return None

    def scrap(self):
        self.tickers_list = self.get_tickers_list()
        self.tickers_to_buy = self.get_new_tickers()
        if self.tickers_to_buy != None:
            self.tickers_list_last = self.tickers_list.copy()

        return self.return_n_clear()
    
    def logger(self, string):
        time = datetime.now()
        log_name = "./logs/log_{}-{}.txt".format(time.day, time.month)
        with open(log_name, "a") as log: # Log the activity
            log.write('{}:{}:{} \n'.format(time.hour, time.minute, time.second))
            log.write(string + '\n\n') 
    
class gateio():
    def __init__(self):
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)
        self.use_api_keys = int(config['GATEIO_API']['use_api_keys'])
        
        self.key = config['GATEIO_API']['key'] if self.use_api_keys else None
        self.secret = config['GATEIO_API']['secret_key'] if self.use_api_keys else None
        
        self.profit = float(config['GENERAL']['profit'])
        
        self.api = self.init_api()
        
    def init_api(self):
        configuration = gate_api.Configuration(
            host = "https://api.gateio.ws/api/v4",
            key = self.key,
            secret = self.secret)
        api_client = gate_api.ApiClient(configuration)
        api_instance = gate_api.SpotApi(api_client)
        return api_instance
    
    def buy_coin(self, ticker):
        currency_pair = ticker + '_USDT'
        is_balance_ok, balance = self.is_balance_ok(currency_pair)
        
        if(is_balance_ok):
            
            # Get the last market price of pair
            tickers = self.api.list_tickers(currency_pair=currency_pair)
            assert len(tickers) == 1
            last_price = tickers[0].last
            
            # Calculate buy price with overprice, to get the order executed immediately
            buy_price = str(float(last_price)*1.01) 
            
            buy_amount = str(float(balance)/(float(buy_price)*1.01)) # Amount of <ticker> to buy
            
            # Create the buy order object
            buy_order = Order(amount=buy_amount, price=buy_price, side='buy', currency_pair=currency_pair, time_in_force='ioc')
            
            try:
                buy_order_resp = self.api.create_order(buy_order)
                order_id = buy_order_resp.id
                
                # Wait for the buy order to be filled or cancelled
                while(buy_order_resp.status not in ['closed', 'cancelled']):
                    sleep(3)
                    buy_order_resp = self.api.get_order(order_id, currency_pair)
                    
                self.logger( '\n' + buy_order_resp.to_str())
                print('\n' + buy_order_resp.to_str())
                
                if (buy_order_resp.status == 'cancelled' and float(buy_order_resp.left) != 0):
                    self.logger('\n' + 'Order was partially filled')
                    print('\n' + 'Order was partially filled')
                    
                
                # Calculate the total amount to sell, discounting the fee and the portion of order that was not filled
                sell_amount = str(float(buy_order_resp.amount) - float(buy_order_resp.left) - float(buy_order_resp.fee))
                # Calculate the sell price based on the buy order data and profit expected
                sell_price = str(float(buy_order_resp.price) * (1 + self.profit))
                 
                sell_order = Order(amount=sell_amount, price=sell_price, side='sell', currency_pair=currency_pair)
                sell_order_resp = self.api.create_order(sell_order)
                
                self.logger(sell_order_resp.to_str())
                print(sell_order_resp.to_str())
                
            except GateApiException as ex:
                print("Gate api exception, label: %s, message: %s\n" % (ex.label, ex.message))
                self.logger("Gate api exception, label: %s, message: %s\n" % (ex.label, ex.message))
            except ApiException as e:
                print("Exception when calling SpotApi->create_order: %s\n" % e)
                self.logger("Exception when calling SpotApi->create_order: %s\n" % e)
        return
        
    def is_balance_ok(self, currency_pair):
        # Check if pair is on gate.io
        try:
            pair = self.api.get_currency_pair(currency_pair)
        except GateApiException as ex:
            print("Gate api exception, label: %s, message: %s\n" % (ex.label, ex.message))
            self.logger("Gate api exception, label: %s, message: %s\n" % (ex.label, ex.message))
            return False, None
        except ApiException as e:
            print("Exception when calling SpotApi->create_order: %s\n" % e)
            self.logger("Exception when calling SpotApi->create_order: %s\n" % e)
            return False, None
        
        # Check if the min amount of token can be buyed with the current balance
        min_amount = float(pair.min_quote_amount)
        accounts = self.api.list_spot_accounts(currency='USDT') 
        assert len(accounts) == 1
        available = accounts[0].available
        
        print("Account available: %s %s" % (available, 'USDT'))
        self.logger("Account available: %s %s" % (available, 'USDT'))
        
        # Check if the balance is available
        if (float(available) < min_amount) or (float(available) < 10):
            print("Account balance not enough")
            self.logger("Account balance not enough")
            return False, None
        else:
            return True, available
        
    def logger(self, string):
        time = datetime.now()
        log_name = "./logs/log_{}-{}.txt".format(time.day, time.month)
        with open(log_name, "a") as log: # Log the activity
            log.write('{}:{}:{} \n'.format(time.hour, time.minute, time.second))
            log.write(string + '\n\n') 

